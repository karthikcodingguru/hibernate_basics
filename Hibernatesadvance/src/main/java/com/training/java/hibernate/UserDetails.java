package com.training.java.hibernate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "user_details")
public class UserDetails {

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getJoined() {
		return Joined;
	}

	public void setJoined(Date joined) {
		Joined = joined;
	}

	private int userId;
	private String userName;
	private String address;

	@Temporal(TemporalType.DATE)
	private Date Joined;

	@Id
	@Column(name = "User_ID")
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Column(name = "User_Name")
	public String getUserName() {
		return userName + "is entered";
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
