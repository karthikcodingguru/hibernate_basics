package com.training.java.hibernate.test;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.training.java.hibernate.UserDetails;

public class UserTest {

	/**
	 * @param args
	 */

	public static void main(String[] args) {

		UserDetails user = new UserDetails();
		user.setUserId(10);
		user.setUserName("Tenthuser");
		user.setAddress("India");
		user.setJoined(new Date());
		SessionFactory sessionFactory = new AnnotationConfiguration()
				.configure().buildSessionFactory();

		Session session = sessionFactory.openSession();

		session.beginTransaction();

		session.save(user);

		session.getTransaction().commit();

		session.close();

		user = null;

		session = sessionFactory.openSession();

		session.beginTransaction();

		user = (UserDetails) session.get(UserDetails.class, 8);

		System.out.println(user.getUserName());
	}
}
