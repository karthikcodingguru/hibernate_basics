package com.java.oracleconnection;
import java.sql.*;

public class OracleConnection {

	public static void main ( String[] args) throws Exception {
		 
		System.out.println("-------- Oracle JDBC Connection Testing ------");
		
		try {
 
			Class.forName("oracle.jdbc.driver.OracleDriver");
 
		} catch (ClassNotFoundException e) {
 
			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return;
 
		}
 
		System.out.println("Oracle JDBC Driver Registered!");
 
		Connection connection = null;
 
		try {
 
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system","oracle12");
 		} catch (SQLException e) {
 
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
 
		}
 
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
	
			
			
			
			connection.close();
			
	
	}
 
}
	

