package com.java.oracleconnection;
import java.sql.*;

public class BasicOracleConn {	

		public static void main(String[] args) throws Exception {
			 
			Connection connection = null;
			Statement statement =  null;
			ResultSet resultset = null; 
			
			try {
	 
				Class.forName("oracle.jdbc.OracleDriver");
	 
	 
				System.out.println("Where is your Oracle JDBC Driver?");
	 

				connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "system","oracle12");
				statement = connection.createStatement(); 
				resultset = statement.executeQuery("select * from parts");

			
				System.out.println("PARTS ID    UNITPRICE    ONHAND");
				while(resultset.next())
				{
				int partsid = resultset. getInt("ID");
				String unitprice = resultset. getString("UNITPRICE");
				String onhand = resultset. getString("ONHAND");
				
								
				System.out.println(partsid + "           " + unitprice+ "             " + onhand);
				
				
				}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
finally{
	
	if(resultset!=null)
	resultset.close();
	
	if(statement!=null)
	statement.close();
	
	connection.close();
	
}
				
		



}
}
	 

		

